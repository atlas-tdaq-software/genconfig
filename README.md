# genconfig

Generate c++ and java code to access OKS data using config package.
See [doxygen](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/production/html/ConfigPackages.html)

```
usage: genconfig [-d | --c++-dir-name directory-name]
                 [-n | --c++-namespace namespace
                 [-i | --c++-headers-dir directory-prefix]
                 [-j | --java-dir-name directory-name]
                 [-p | --java-package-name package-name]
                 [-I | --include-dirs dirs*]
                 [-c | --classes class*]
                 [-D | --user-defined-classes [namespace::]user-class[@dir-prefix]*]
                 [-f | --info-file-name file-name]
                 [-v | --verbose]
                 [-h | --help]
                 -s | --schema-files file.schema.xml+

Options/Arguments:
       -d directory-name    name of directory for c++ header and implementation files
       -n namespace         namespace for c++ classes
       -i directory-prefix  name of directory prefix for c++ header files
       -j directory-name    name of directory for java files
       -p package-name      package name for java files
       -I dirs*             directories where to search for already generated files
       -c class*            explicit list of classes to be generated
       -D [x::]c[@d]*       user-defined classes
       -f filename          name of output file describing generated files
       -v                   switch on verbose output
       -h                   this message
       -s files+            the schema files (at least one is mandatory)

Description:
       The utility generates c++ and java code for OKS schema files.
```

## Mapping

Attributes are mapped to appropriate primitive types of C++.
Relations are mapped to pointers to objects.

An accessor function is generated for each attribute and relation. 

Multipe attributes or relations a mapped to a vector of objects. Those vectors should not be copied (although you can), but accessed via operator[] or iterators:
```
for(size_t i = 0; i < obj.attr().size(); i++) {
  int x = obj.attr()[i];
} 
```

or like this for relations of type 'Relation'
```
for(RelationIterator it = obj.rel().begin(); it != obj.rel().end(); ++it) {
  const Relation *p = *it;
}
```
For each generated class type 'X' there is a typedef for 'XIterator' to access multiple relations to that type.

## Accessing objects

Objects can be directly accessed via a 'Configuration' object:
```
const MyData *myData = conf.get<MyData>("name");
```
or
```
std::vector<const MyData*> mydata = conf.get<MyData>();
```

to get all objects of the given type. When the database is unloaded (or destroyed) all objects are automatically freed. The user should not delete any objects himself (actually the destructor is private, so he can't...)

## Testing

To test this package, type 'make' first, then go to the 'test' directory and type
```
   % ../${CMTCONFIG}/genconfig.exe okstest.schema.xml
```
followed by a `make test` in `cmt`. 
```
   % cd ../test
   % setenv TDAQ_DB_SCHEMA okstest.schema.xml
   % ../${CMTCONFIG}/test_gen.exe okstest.data.xml
```

Output should be:

```
Found 'primitive'
TestPrimitive
prim.uc  = 0
prim.b   = 1
prim.us  = 1234
prim.ss  = -1234
prim.ui  = 123456
prim.si  = -123456
prim.f   = 3.141
prim.d   = -31.41
prim.str =
Found 'multiple'
TestMultiple
mult.ui[0] = 1234
mult.ui[1] = 5678
mult.str[0] = hello
mult.str[1] = world
Found 'relation'
TestRelation
TestPrimitive
prim.uc  = 0
prim.b   = 1
prim.us  = 1234
prim.ss  = -1234
prim.ui  = 123456
prim.si  = -123456
prim.f   = 3.141
prim.d   = -31.41
prim.str = hello world
TestMultiple
mult.ui[0] = 1234
mult.ui[1] = 5678
mult.str[0] = hello
mult.str[1] = world
TestMultiple
mult.ui[0] = 1
mult.ui[1] = 2
mult.str[0] = how
mult.str[1] = are
mult.str[2] = you
```
